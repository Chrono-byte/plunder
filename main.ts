import Host from "./server.ts";
import { load } from "https://deno.land/std@0.224.0/dotenv/mod.ts";

const envVars = await load();

const host = new Host({
  displayName: "My Server",
  port: 8080,
});

host.start();
