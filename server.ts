import { Prisma, PrismaClient } from "./generated/client/deno/edge.ts";

interface LaunchOptions {
  port: number;
  displayName: string;
  public: boolean;
}

interface PlunderObject {
  id: string; // unique identifier
}

interface User extends PlunderObject {
  // public info
  displayName: string; // display name
  handle: string; // public handle

  // public bio-type info
  avatar: {
    URL: string; // silly
    altText: string; // e.g. "Anime girl with pink hair"
  };
  biography: string; // user bio
  pronouns: string; // e.g. ["she", "her"]
  location: string; // Custom location
  localTimezone: string; // stored as IANA timezone string
  status: {
    status: string; // online, offline, away, etc.
    message: string; // custom status message
  }; // online, offline, away, etc. + custom status message

  // private/authentication info
  _email: string; // email address
  _token: string; // token
  _lastActive: Date; // last time user was active (last user action timestamp)
}

interface Message extends PlunderObject {
  content: string;
  attachments: string[]; // URLs to attachments
}

interface Thread extends PlunderObject {
  name: string; // channel display name

  _messages: Message[]; // messages in the channel
}

interface Guild extends PlunderObject {
  name: string;
  icon: string;
  channels: Thread[]; //
  members: string[];
}

interface SocketMessage {
  directive: string; // e.g. "SEND_MESSAGE", "EDIT_MESSAGE", "DELETE_MESSAGE"
  body: {
    content?: string;
    attachments?: string[];
    destination?: {
      guild: string;
      channel: string;
    };
  };
}

function handleSocket(socket: WebSocket) {
  socket.addEventListener("open", () => {
    console.log("a client connected!");
  });

  socket.addEventListener("message", (event) => {
    const message = JSON.parse(event.data) as SocketMessage;

    console.log("Message received:", message);
  });
}

export default class Host {
  _guilds: Guild[];
  _users: User[];
  _prisma: PrismaClient;
  constructor(props: LaunchOptions) {
    console.log(`Starting ${props.displayName} on port ${props.port}`);

    // add private collection of guilds and users
    this._guilds = [];
    this._users = [];

    // start database connection
    const prisma = new PrismaClient();

    // assign connection to instance
    this._prisma = prisma;
  }

  _sync() {
    // sync guilds and users with database
  }

  start() {
    console.log("Starting server...");

    // create a default user
    const user: User = {
      displayName: "Test User",
      handle: "testuser",
      id: "testuser",
      avatar: {
        URL: "https://example.com/avatar.png",
        altText: "A generic avatar",
      },
      biography: "A test user",
      pronouns: "they/them",
      location: "Earth",
      localTimezone: "America/New_York",
      status: {
        status: "online",
        message: "Just testing",
      },
      _email: "",
      _token: "",
      _lastActive: new Date(),
    };

    // add user to collection
    this._users.push(user);

    console.log("User created:", this._users.find((u) => u.id === "testuser"));

    Deno.serve((req) => {
      if (req.headers.get("upgrade") != "websocket") {
        return new Response(null, { status: 501 });
      }

      const { socket, response } = Deno.upgradeWebSocket(req);

      handleSocket(socket);

      return response;
    });
  }
}
